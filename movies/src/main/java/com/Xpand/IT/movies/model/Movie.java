package com.Xpand.IT.movies.model;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity(name = "movie")
public class Movie implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false, updatable = false)
    private Long id;
    private String title;
    private int revenue;
    private int year;

    public Movie() {
    }

    public Movie(String title, int revenue, int year) {
        this.title = title;
        this.revenue = revenue;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRevenue() {
        return revenue;
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Movies{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", revenue=" + revenue +
                ", year=" + year +
                '}';
    }
}