package com.Xpand.IT.movies.repo;

import com.Xpand.IT.movies.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MovieRepo extends JpaRepository<Movie, Long> {
    Optional<List<Movie>> findMoviesByYear(int year);

    Optional<Movie> findMovieById(long id);

    @Query(value= "SELECT * from movie order by revenue desc limit 10", nativeQuery = true)
    List<Movie> findTop10MoviesOrderByRevenue();

    @Query(value="SELECT * from movie where year = :ano order by revenue desc limit 10", nativeQuery = true)
    Optional<List<Movie>> findTop10MoviesOrderByRevenueByYear(@Param("ano")int year);

    @Query(value="Select * from movie limit 22 offset :desvio", nativeQuery = true)
    List<Movie> findAllMovies(@Param("desvio")int desvio);
}
