package com.Xpand.IT.movies.service;

import com.Xpand.IT.movies.exception.MoviesNotFound;
import com.Xpand.IT.movies.model.Movie;
import com.Xpand.IT.movies.repo.MovieRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {
    private final MovieRepo movieRepo;

    @Autowired
    public MovieService(MovieRepo movieRepo) {
        this.movieRepo = movieRepo;
    }

    public List<Movie> findAllMovies() {
        return movieRepo.findAll();
    }

    public List<Movie> findAllMovies(int desvio) {
        return movieRepo.findAllMovies(desvio);
    }

    public Movie findMovieById(long id) {
        return movieRepo.findMovieById(id)
                .orElseThrow(()-> new MoviesNotFound(
                "No Movie found for the id:" + id));
    }

    public List<Movie> findTop10MoviesOrderByRevenue() {
        return movieRepo.findTop10MoviesOrderByRevenue();
    }

    public List<Movie> findMoviesByYear(int year) {
        return movieRepo.findMoviesByYear(year)
                .orElseThrow(()-> new MoviesNotFound(
                        "No Movies found for the year " + year));
    }

    public List<Movie> findTop10MoviesOrderByRevenueByYear(int year) {
        return movieRepo.findTop10MoviesOrderByRevenueByYear(year)
                .orElseThrow(()-> new MoviesNotFound(
                "No Movies found for the year " + year));
    }
}
