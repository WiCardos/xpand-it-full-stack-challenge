package com.Xpand.IT.movies.exception;

public class MoviesNotFound extends RuntimeException {
    public MoviesNotFound(String message) {
        super(message);
    }
}
