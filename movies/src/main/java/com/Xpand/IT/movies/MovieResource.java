package com.Xpand.IT.movies;

import com.Xpand.IT.movies.model.Movie;
import com.Xpand.IT.movies.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/movie")
public class MovieResource {
    private final MovieService movieService;

    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }

    //Required for RQ001
    @GetMapping("/all")
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieService.findAllMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    //Required for RQ001 for infinite scroll
    @GetMapping("/all/{desvio}")
    public ResponseEntity<List<Movie>> getAllMovies(@PathVariable("desvio") int desvio) {
        List<Movie> movies = movieService.findAllMovies(desvio);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    //Might be required for RQ002
    @GetMapping("/byId/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable("id") long id){
        Movie movie = movieService.findMovieById(id);
        return new ResponseEntity<>(movie, HttpStatus.OK);
    }

    //Required for RQ003
    @GetMapping("/top10MoviesOrderByRevenue")
    public ResponseEntity<List<Movie>> getTop10MoviesOrderByRevenue() {
        List<Movie> movies = movieService.findTop10MoviesOrderByRevenue();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    //Not required. Created for service testing by id.
    @GetMapping("/byYear/{year}")
    public ResponseEntity<List<Movie>> getMoviesByYear(@PathVariable("year") int year) {
        List<Movie> movies = movieService.findMoviesByYear(year);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    //Required for RQ003
    @GetMapping("/top10MoviesOrderByRevenueByYear/{year}")
    public ResponseEntity<List<Movie>> getTop10MoviesOrderByRevenueByYear(@PathVariable("year")int year) {
        List<Movie> movies = movieService.findTop10MoviesOrderByRevenueByYear(year);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
}
