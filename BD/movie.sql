-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2024 at 01:08 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinel2024`
--

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id` bigint(20) NOT NULL,
  `revenue` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `revenue`, `title`, `year`) VALUES
(1, 1711701, 'movie_a', 2022),
(2, 2961971, 'movie_b', 1994),
(3, 872140, 'movie_c', 1998),
(4, 9912935, 'movie_d', 1950),
(5, 496517, 'movie_e', 1967),
(6, 217414, 'movie_f', 1980),
(7, 9662211, 'movie_g', 1995),
(8, 1625290, 'movie_h', 2023),
(9, 3920623, 'movie_i', 1952),
(10, 9864230, 'movie_j', 2012),
(11, 2134778, 'movie_k', 1992),
(12, 1782708, 'movie_l', 1965),
(13, 4658470, 'movie_m', 2004),
(14, 2466564, 'movie_n', 1953),
(15, 4938485, 'movie_o', 1974),
(16, 1632157, 'movie_p', 2012),
(17, 6564617, 'movie_q', 2009),
(18, 38323, 'movie_r', 1997),
(19, 1646589, 'movie_s', 2018),
(20, 940285, 'movie_t', 2003),
(21, 3046121, 'movie_u', 1977),
(22, 9413738, 'movie_v', 1994),
(23, 1445934, 'movie_w', 2020),
(24, 2749123, 'movie_x', 1991),
(25, 9219940, 'movie_y', 2022),
(26, 436764, 'movie_z', 1974),
(27, 5319054, 'movie_zz1', 2010),
(28, 6617665, 'movie_zz2', 2010),
(29, 7131163, 'movie_zz3', 2010),
(30, 5802820, 'movie_zz4', 2010),
(31, 7620613, 'movie_zz5', 2010),
(32, 4352136, 'movie_a1', 1972),
(33, 1753641, 'movie_b1', 2024),
(34, 4149959, 'movie_c1', 1958),
(35, 3066459, 'movie_d1', 1965),
(36, 913107, 'movie_e1', 2013),
(37, 9750896, 'movie_f1', 1974),
(38, 7049075, 'movie_g1', 1990),
(39, 6174368, 'movie_h1', 1983),
(40, 3871325, 'movie_i1', 1994),
(41, 7985865, 'movie_j1', 1966),
(42, 6889514, 'movie_k1', 2009),
(43, 9027995, 'movie_l1', 1959),
(44, 9526280, 'movie_m1', 1977),
(45, 9747379, 'movie_n1', 2008),
(46, 9463680, 'movie_o1', 1980),
(47, 2083514, 'movie_p1', 2010),
(48, 4413662, 'movie_q1', 2007),
(49, 5107951, 'movie_r1', 1968),
(50, 7328113, 'movie_s1', 2017),
(51, 3262598, 'movie_t1', 2018),
(52, 6036360, 'movie_u1', 1970),
(53, 5327465, 'movie_v1', 2014),
(54, 6894602, 'movie_w1', 2015),
(55, 3050262, 'movie_x1', 2017),
(56, 5903558, 'movie_y1', 1968),
(57, 4715259, 'movie_z1', 1995);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
