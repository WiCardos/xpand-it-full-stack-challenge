# Xpand IT Full Stack Challenge

## Name
Xpand IT Full Stack Challenge

## Description
The goal is to fulfill Mark's requirements in the Xpand It Full Stack Challenge. I am going to start by attempting to make an application that fulfill completely RQ001, and then move on to the following requirements.
(Actually, I achieved RQ003 and RQ002 before RQ001)

If possible, I'll have all services necessary built in the back-end and then focus on the front-end. (Achieved).

I focused on doing a minimal viable product. There is always going to be a better, faster, prettier way of doing, but, at this time, my focus is to have a minimalist working solution.

I am using MySQL, Java Spring and Angular.

I managed to implement RQ002 in all lists of movies.

I fixed the infinite scroll to work properly with offset queries.

## MySQL
For MySql I am using Xampp's MySQL that I already had from a previous installation.
In the folder BD there are 2 files: BD_commands.txt and movie.sql. The first one has the commands I ran, and the later file is an export from MySQL.

## Spring Dependencies:
Spring Web
Spring Data JPA
MySQL Driver.

## Angular
I used ngx-infinite-scroll for the infinite scroll feature.
I used bootstrap for the popup in RQ002.

## Bibliography/Videograhy
https://youtu.be/Gx4iBLKLVHk
https://youtu.be/G4X17MbCc_g

## Authors and acknowledgment
Wilson Pontes Cardoso