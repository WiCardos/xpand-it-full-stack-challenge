import {Observable} from 'rxjs';
import {Movie} from './movie';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MovieService{
    private apiServerUrl = environment.apiBaseUrl;

    constructor(private http: HttpClient){}

    public getMovies(): Observable<Movie[]>{
        return this.http.get<Movie[]>(`${this.apiServerUrl}/movie/all`);
    }

    public getMoviesOffset(desvio: number): Observable<Movie[]>{
        return this.http.get<Movie[]>(`${this.apiServerUrl}/movie/all/${desvio}`);
    }

    public getTop10Movies(): Observable<Movie[]>{
        return this.http.get<Movie[]>(`${this.apiServerUrl}/movie/top10MoviesOrderByRevenue`);
    }

    public getTop10MoviesByYear(year: number): Observable<Movie[]>{
        return this.http.get<Movie[]>(`${this.apiServerUrl}/movie/top10MoviesOrderByRevenueByYear/${year}`);
    }
}