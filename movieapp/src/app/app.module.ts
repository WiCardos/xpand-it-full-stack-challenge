import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { MovieService } from "./movie.service";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
    declarations:   [
        // AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        InfiniteScrollModule
    ],
    providers: [MovieService]
    // ,
    // bootstrap: [AppComponent]
})
export class AppModule {}