import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import {Movie}  from './movie';
import {MovieService} from './movie.service';
import {HttpErrorResponse} from '@angular/common/http';
import { OnInit } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, InfiniteScrollModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  // title = 'movieapp';
  public movies: Movie[];
  public topMovies: Movie[];
  public topYearMovies: Movie[];
  public movie: Movie;
  desvio: number = 0;

  constructor(private movieService: MovieService){
    //is this ok?
    this.movies = [];
    this.topMovies = [];
    this.topYearMovies = [];
    this.movie = {
      id: 0,
      title: "",
      revenue: "",
      year: 0}
  }

  ngOnInit() {
    this.getAllMovies(this.desvio);
    this.getTopMovies();
  }


  //---------------
  //Show all movies
  //---------------
  showAllMovies: boolean = false;
  onClickAllMovies() {
    this.showAllMovies = !this.showAllMovies
    if (this.showTopMovies) {
      this.showTopMovies = false
    }
    if (this.showTopMoviesByYear) {
      this.showTopMoviesByYear = false
    }
  }

  

  public getAllMovies(desvio: number): void{
    this.movieService.getMoviesOffset(desvio).subscribe(
      (response: Movie[]) => {
        this.movies.push.apply(this.movies, response);
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    );
  }
  noMoreMovies: boolean = false;
  onScroll(){

    this.desvio += 22;
    this.getAllMovies(this.desvio);

    if(this.movies.length<this.desvio ){
      this.noMoreMovies=true;
    }

  }


  //-----------------------------------
  //Show Top 10 movies order by revenue
  //-----------------------------------
  showTopMovies: boolean = false;
  onClickTopMovies() {
    this.showTopMovies = !this.showTopMovies
    if (this.showAllMovies) {
      this.showAllMovies = false
    }
    if (this.showTopMoviesByYear) {
      this.showTopMoviesByYear = false
    }
  }
  public getTopMovies(): void {
    this.movieService.getTop10Movies().subscribe(
      (response: Movie[]) => {
        this.topMovies = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }


  //----------------------------------------------------
  //Show Top 10 movies order by revenue filtered by year
  //----------------------------------------------------
  showTopMoviesByYear: boolean = false;

  intyear: number = 0;
  public getTopYearMovies(year: string): void{

    this.intyear = parseInt(year);

    //To prevent error messages while the user is inputting the year.
    if(isNaN(this.intyear)){
      this.intyear = 0;
    }

    this.showTopMoviesByYear=true;
    if (this.showAllMovies) {
      this.showAllMovies = false
    }
    if (this.showTopMovies) {
      this.showTopMovies = false
    }

    this.movieService.getTop10MoviesByYear(this.intyear).subscribe(
      (response: Movie[]) => {
        this.topYearMovies = response;
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    );
  }


  
  //----------
  //Show PopUp
  //----------
  openModel(movie: Movie) {
    this.movie = movie;
    const modelDiv = document.getElementById('movieDetails')
    if(modelDiv!=null){
      modelDiv.style.display = 'block';
    }
  }

  closeModel() {
    const modelDiv = document.getElementById('movieDetails')
    if(modelDiv!=null){
      modelDiv.style.display = 'none';
    }
  }
}

