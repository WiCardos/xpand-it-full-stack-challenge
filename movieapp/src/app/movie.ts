export interface Movie{
    id: number;
    title: string;
    revenue: string;
    year: number;
}